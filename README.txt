Branding site name.
Allows setting alternative site name for Branding region.

Sometimes site-builder wants to change 'Site name' text which displays in
'Branding' region of page (it's where Logo & Slogan placed too) and leave
existing one in other places like <head> title.
For example your site name is 'Company "NAME"' and you want that site name to be
in <head> title like 'About Us | Company "NAME"'. But on the page you don't want
to see the whole 'Company "NAME"' but just NAME of your Company in 'Branding'
region.
This module lets you do that. After installing it you'll find 'Branding site
name' text field on Admin > Config > System > Site information page. While it's
blank original Site name will be used.
Also there's a checkbox that lets you hide a site slogan in title bar. It is
shown there by default on pages which have no own title.
There's no other configuration.
